import "./styles.css";
import i18n from "i18next";
import { useTranslation } from "react-i18next";

export default function LanguageSelect() {
  const { t } = useTranslation();
  return (
    <div className="dropdown">
      <span>{t("Language")}</span>
      <div className="dropdown-content">
        <p onClick={() => i18n.changeLanguage("ro")}>Romana</p>
        <p onClick={() => i18n.changeLanguage("en")}>English</p>
      </div>
    </div>
  );
}
