import "./styles.css";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
export default function Acasa() {
  const { t } = useTranslation();

  return (
    <div style={{ height: "100vh" }}>
      <h1 style={{ fontFamily: "Comfortaa", padding: "25px" }}>
        {t("Welcome to DentaWeb")}
      </h1>
      <div>
        <p
          style={{
            fontFamily: "RobotoCondensed",
            fontSize: "20px",
            color: "#2D2D3A",
            padding: "10px"
          }}
        >
          {t("Home_text_1")}
        </p>
        <p
          style={{
            fontFamily: "RobotoCondensed",
            fontSize: "20px",
            padding: "10px"
          }}
        >
          {t("Home_text_2")}
        </p>
        <p
          style={{
            fontFamily: "RobotoCondensed",
            fontSize: "20px",
            padding: "10px"
          }}
        >
          {t("Home_text_3")}
        </p>
        <p
          style={{
            fontFamily: "RobotoCondensed",
            fontSize: "20px",
            padding: "10px"
          }}
        >
          {t("Home_text_4")}
        </p>
        <div>
          <Link
            to="/ServiciiTarife"
            style={{ padding: "10px", margin: "10px" }}
            className="btn btn-primary"
          >
            {t("Services")}
          </Link>
          <Link
            to="/DespreNoi"
            style={{ padding: "10px", margin: "10px" }}
            className="btn btn-primary"
          >
            {t("About Us")}
          </Link>
          <Link
            to="/Medici"
            style={{ padding: "10px", margin: "10px" }}
            className="btn btn-primary"
          >
            {t("Doctors")}
          </Link>
          <Link
            to="/contact"
            style={{ padding: "10px", margin: "10px" }}
            className="btn btn-primary"
          >
            Contact
          </Link>
          <Link
            to="/Noutati"
            style={{ padding: "10px", margin: "10px" }}
            className="btn btn-primary"
          >
            {t("News")}
          </Link>
        </div>
      </div>
    </div>
  );
}
