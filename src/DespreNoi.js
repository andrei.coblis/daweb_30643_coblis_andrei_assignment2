import { useTranslation } from "react-i18next";
export default function DespreNoi() {
  const { t } = useTranslation();
  return (
    <div>
      <h1 style={{ fontFamily: "Comfortaa", padding: "20px" }}>
        {t("About Us")}
      </h1>
      <div
        style={{
          backgroundColor: "#20A0D8",
          color: "white",
          marginRight: "200px",
          marginLeft: "200px",
          fontFamily: "Comfortaa"
        }}
      >
        {t("DespreNoi_title_1")}
      </div>
      <p
        style={{
          fontFamily: "RobotoCondensed",
          fontSize: "20px",
          color: "#2D2D3A",
          marginRight: "200px",
          marginLeft: "200px",
          backgroundColor: "white"
        }}
      >
        {t("DespreNoi_text_1")}
      </p>
      <div
        style={{
          backgroundColor: "#20A0D8",
          color: "white",
          marginRight: "200px",
          marginLeft: "200px",
          fontFamily: "Comfortaa"
        }}
      >
        {t("DespreNoi_title_2")}
      </div>
      <p
        style={{
          fontFamily: "RobotoCondensed",
          fontSize: "20px",
          color: "#2D2D3A",
          marginRight: "200px",
          marginLeft: "200px",
          backgroundColor: "white"
        }}
      >
        {t("DespreNoi_text_2")}
      </p>
      <div
        style={{
          backgroundColor: "#20A0D8",
          color: "white",
          marginRight: "200px",
          marginLeft: "200px",
          fontFamily: "Comfortaa"
        }}
      >
        {t("DespreNoi_title_3")}
      </div>
      <p
        style={{
          fontFamily: "RobotoCondensed",
          fontSize: "20px",
          color: "#2D2D3A",
          marginRight: "200px",
          marginLeft: "200px",
          backgroundColor: "white"
        }}
      >
        {t("DespreNoi_text_3")}
      </p>
      <div
        style={{
          backgroundColor: "#20A0D8",
          color: "white",
          marginRight: "200px",
          marginLeft: "200px",
          fontFamily: "Comfortaa"
        }}
      >
        {t("DespreNoi_title_4")}
      </div>
      <p
        style={{
          fontFamily: "RobotoCondensed",
          fontSize: "20px",
          color: "#2D2D3A",
          marginRight: "200px",
          marginLeft: "200px",
          backgroundColor: "white"
        }}
      >
        {t("DespreNoi_text_4")}
      </p>

      <iframe
        title="COVID presentation"
        style={{ marginTop: "30px", marginRight: "200px", marginLeft: "200px" }}
        height="400"
        width="50%"
        src="https://www.youtube.com/embed/uK2OE48V8X8"
      ></iframe>
    </div>
  );
}
