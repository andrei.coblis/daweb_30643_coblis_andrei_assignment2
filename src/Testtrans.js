import { useTranslation } from "react-i18next";

export function Test() {
  const { t } = useTranslation();

  return <p>{t("translation")}</p>;
}
