import "./styles.css";
import { Route, Switch, Link, BrowserRouter as Router } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import Home from "./Home";
import Contact from "./Contact";
import DespreNoi from "./DespreNoi";
import Medici from "./Medici";
import Noutati from "./Noutati";
import ServiciiTarife from "./ServiciiTarife";
import LanguageSelect from "./languageSelect";
import React, { Suspense } from "react";
import { useTranslation } from "react-i18next";

export default function App() {
  const { t } = useTranslation();

  return (
    <div className="App">
      <Suspense fallback="loading">
        <header>
          <Router>
            <div className="nav-bar">
              <ul>
                <li className="nav-bar-item" id="nav-bar-logo">
                  <img src={require("/img/dentaWeb.png")} alt="logo"></img>
                </li>
                <li className="nav-bar-item" id="nav-bar-home">
                  <Link to="/">{t("Home")}</Link>
                </li>
                <li className="nav-bar-item">
                  <LanguageSelect />
                </li>
                <li className="nav-bar-item">
                  <Link to="/Noutati">{t("News")}</Link>
                </li>
                <li className="nav-bar-item">
                  <Link to="/Contact">Contact</Link>
                </li>
                <li className="nav-bar-item">
                  <Link to="/Medici">{t("Doctors")}</Link>
                </li>
                <li className="nav-bar-item">
                  <Link to="/DespreNoi">{t("About Us")}</Link>
                </li>
                <li className="nav-bar-item">
                  <Link to="/ServiciiTarife">{t("Services")}</Link>
                </li>
              </ul>
            </div>
            <Switch>
              <Route exact path="/">
                <Home />
              </Route>
              <Route exact path="/Noutati">
                <Noutati />
              </Route>
              <Route exact path="/Contact">
                <Contact />
              </Route>
              <Route exact path="/DespreNoi">
                <DespreNoi />
              </Route>
              <Route exact path="/Medici">
                <Medici />
              </Route>
              <Route exact path="/ServiciiTarife">
                <ServiciiTarife />
              </Route>
            </Switch>
          </Router>
        </header>
      </Suspense>
    </div>
  );
}
