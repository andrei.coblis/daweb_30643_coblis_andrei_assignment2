from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.EmailView.as_view()),
]
