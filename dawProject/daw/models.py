from django.db import models

class Email(models.Model):
    name = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    subject = models.CharField(max_length=100)
    content = models.CharField(max_length=100)
