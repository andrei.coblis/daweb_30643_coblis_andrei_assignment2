from django.shortcuts import render
from django.conf import settings
from django.core.mail import send_mail
from . serializers import EmailSerializer
from . models import Email
from rest_framework import generics

# Create your views here.
class EmailView(generics.ListCreateAPIView):
    queryset = Email.objects.all()
    serializer_class = EmailSerializer
    name = Email.objects.latest("pk").name
    subject = Email.objects.latest("pk").subject
    content = Email.objects.latest("pk").content
    email_from = Email.objects.latest("pk").email
    receipient_list = ['imanuel.kanter@gmail.com']
    send_mail(subject, content, email_from, receipient_list)
    